package com.twuc.webApp.repo;

import com.twuc.webApp.ApiTestBase;
import com.twuc.webApp.entity.Rob;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;

import javax.persistence.EntityManager;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class RobRepoTest extends ApiTestBase {

    @Autowired
    EntityManager entityManager;

    @Autowired
    RobRepo robRepo;

    @Test
    void should_return_rob_when_add_rob() {
        Rob rob = new Rob("Rob", "Hall");

        Rob savedRob = robRepo.saveAndFlush(rob);
        entityManager.clear();

        Optional<Rob> robOptional = robRepo.findById(savedRob.getId());
        assertEquals(rob, robOptional.get());
    }

    @Test
    void should_throw_exception_when_fisrt_name_length_greater_64() {
        Rob rob = new Rob("RobbbbbbbbRobbbbbbbbRobbbbbbbbRobbbbbbbbRobbbbbbbbRobbbbbbbbRobbbbbbbbRobbbbbbbbRobbbbbbbb", "Hall");

        assertThrows(RuntimeException.class, () -> {
            robRepo.saveAndFlush(rob);
        });
    }

    @Test
    void should_throw_exception_when_last_name_length_greater_64() {
        Rob rob = new Rob("Rob", "RobbbbbbbbRobbbbbbbbRobbbbbbbbRobbbbbbbbRobbbbbbbbRobbbbbbbbRobbbbbbbbRobbbbbbbbRobbbbbbbb");

        assertThrows(RuntimeException.class, () -> {
            robRepo.saveAndFlush(rob);
        });
    }

    @Test
    void should_return_rob_when_input_rob_id() {
        Rob rob = new Rob("Rob", "Hall");
        Rob savedRob = robRepo.saveAndFlush(rob);
        entityManager.clear();

        Optional<Rob> robOptional = robRepo.findById(rob.getId());

        assertEquals(Long.valueOf(1), savedRob.getId());
        assertEquals("Rob", savedRob.getFirstName());
        assertEquals("Hall", savedRob.getLastName());
    }

    @Test
    void should_all_rob_when_get_all_rob() {
        Rob rob1 = new Rob("Rob1", "Hall1");
        Rob rob2 = new Rob("Rob2", "Hall2");
        robRepo.saveAll(Arrays.asList(rob1, rob2));
        robRepo.flush();
        entityManager.clear();

        List<Rob> robs = robRepo.findAll(Sort.by("id"));

        assertEquals(2, robs.size());
        assertEquals(rob1, robs.get(0));
        assertEquals(rob2, robs.get(1));
    }

    @Test
    void should_0_rob_when_get_all_robs_but_database_non() {
        List<Rob> robs = robRepo.findAll(Sort.by("id"));
        assertEquals(0, robs.size());
    }

    @Test
    void should_return_zoneId_when_save_rob_with_zone_id_after_get_rob() {
        Rob rob = new Rob("Rob", "Hall");
        rob.setZoneId("Asia/Chongqing");
        Rob saveRob = robRepo.saveAndFlush(rob);
        entityManager.clear();

        Optional<Rob> robOptional = robRepo.findById(saveRob.getId());

        assertEquals(rob.getZoneId(), robOptional.get().getZoneId());
    }

}
