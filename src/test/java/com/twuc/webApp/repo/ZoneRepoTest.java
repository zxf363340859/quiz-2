package com.twuc.webApp.repo;

import com.twuc.webApp.ApiTestBase;
import com.twuc.webApp.entity.Zone;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;

import javax.persistence.EntityManager;
import java.time.zone.ZoneRulesProvider;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ZoneRepoTest extends ApiTestBase {

    @Autowired
    ZoneRepo zoneRepo;

    @Autowired
    EntityManager entityManager;

    @Test
    void should_return_zone_order_by_char_asc_when_get_all_zone() {
        Set<String> availableZoneIds = ZoneRulesProvider.getAvailableZoneIds();
        List<Zone> zones = new ArrayList<>();
        availableZoneIds.forEach(z -> zones.add(new Zone(z)));

        List<Zone> s = zoneRepo.saveAll(zones);
        zoneRepo.flush();

        List<Zone> zoneIds = zoneRepo.findAll(Sort.by("zoneId"));
        assertEquals(availableZoneIds.size(), zoneIds.size());
    }
}
