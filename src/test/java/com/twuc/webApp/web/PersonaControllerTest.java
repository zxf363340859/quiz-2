package com.twuc.webApp.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.ApiTestBase;
import com.twuc.webApp.entity.Rob;
import com.twuc.webApp.entity.Zone;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.time.zone.ZoneRulesProvider;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class PersonaControllerTest extends ApiTestBase{

    @Autowired
    MockMvc mockMvc;

    @Test
    void should_return_201_and_Location_when_add_Rob_user() throws Exception {
        Rob rob = new Rob("Rob", "Hall");
        String serializedRob = serialize(rob);
        mockMvc.perform(post("/api/staffs").contentType(MediaType.APPLICATION_JSON).content(serializedRob))
            .andExpect(status().is(HttpStatus.CREATED.value()))
            .andExpect(header().string("Location", "/api/staffs/1"));
    }

    @Test
    void should_return_400_when_first_name_length_greater_64() throws Exception {
        Rob rob = new Rob("RobbbbbbbbRobbbbbbbbRobbbbbbbbRobbbbbbbbRobbbbbbbbRobbbbbbbbRobbbbbbbbRobbbbbbbbRobbbbbbbb", "Hall");
        String serializedRob = serialize(rob);
        mockMvc.perform(post("/api/staffs").contentType(MediaType.APPLICATION_JSON).content(serializedRob))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()));
    }

    @Test
    void should_return_400_when_last_name_length_greater_64() throws Exception {
        Rob rob = new Rob("rob", "RobbbbbbbbRobbbbbbbbRobbbbbbbbRobbbbbbbbRobbbbbbbbRobbbbbbbbRobbbbbbbbRobbbbbbbbRobbbbbbbb");
        String serializedRob = serialize(rob);
        mockMvc.perform(post("/api/staffs").contentType(MediaType.APPLICATION_JSON).content(serializedRob))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()));
    }

    @Test
    void should_return_rob_when_input_robs_id() throws Exception {
        Rob rob = new Rob("Rob", "Hall");
        String serializedRob = serialize(rob);
        mockMvc.perform(post("/api/staffs").contentType(MediaType.APPLICATION_JSON).content(serializedRob))
                .andExpect(status().is(HttpStatus.CREATED.value()))
                .andExpect(header().string("Location", "/api/staffs/1"));

        mockMvc.perform(get("/api/staffs/1"))
                .andExpect(status().is(HttpStatus.OK.value()))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.firstName").value("Rob"))
                .andExpect(jsonPath("$.lastName").value("Hall"));
    }

    @Test
    void should_return_404_when_input_robs_id_that_database_have_not_robs_id() throws Exception {
        mockMvc.perform(get("/api/staffs/1"))
                .andExpect(status().is(HttpStatus.NOT_FOUND.value()));
    }

    @Test
    void get_all_robs() throws Exception {
        Rob rob1 = new Rob("Rob1", "Hall1");
        String serializedRob1 = serialize(rob1);
        Rob rob2 = new Rob("Rob1", "Hall1");
        String serializedRob2 = serialize(rob1);
        mockMvc.perform(post("/api/staffs").contentType(MediaType.APPLICATION_JSON).content(serializedRob1));
        mockMvc.perform(post("/api/staffs").contentType(MediaType.APPLICATION_JSON).content(serializedRob2));

        String contentAsString = mockMvc.perform(get("/api/staffs")).
                andReturn().getResponse().getContentAsString();
        ObjectMapper objectMapper = new ObjectMapper();
        List<Rob> list = objectMapper.readValue(contentAsString, ArrayList.class);
        assertEquals(2, list.size());
    }

    @Test
    void should_return_null_json_array_when_get_all_robs_but_database_have_not_rob() throws Exception {
        mockMvc.perform(get("/api/staffs"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string("[]"));
    }

    @Test
    void should_return_200_when_set_zone_id_for_rob() throws Exception {
        Rob rob = new Rob("Rob", "Hall");
        String serializedRob = serialize(rob);
        mockMvc.perform(post("/api/staffs").contentType(MediaType.APPLICATION_JSON).content(serializedRob));

        Zone zone = new Zone();
        zone.setZoneId("Asia/Chongqing");
        String serializedZone = serialize(zone);
        mockMvc.perform(put("/api/staffs/1/timezone").contentType(MediaType.APPLICATION_JSON).content(serializedZone))
                .andExpect(status().is(HttpStatus.OK.value()));
    }

    @Test
    void should_return_400_when_no_zone_id_() throws Exception {
        Rob rob = new Rob("Rob", "Hall");
        String serializedRob = serialize(rob);
        mockMvc.perform(post("/api/staffs").contentType(MediaType.APPLICATION_JSON).content(serializedRob));

        mockMvc.perform(put("/api/staffs/1/timezone").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()));

    }

    @Test
    void should_return_400_when_zone_id_incorrect() throws Exception {
        Rob rob = new Rob("Rob", "Hall");
        String serializedRob = serialize(rob);
        mockMvc.perform(post("/api/staffs").contentType(MediaType.APPLICATION_JSON).content(serializedRob));

        Zone zone = new Zone();
        zone.setZoneId("Asia/chongqing");
        String serializedZone = serialize(zone);
        mockMvc.perform(put("/api/staffs/1/timezone").contentType(MediaType.APPLICATION_JSON).content(serializedZone))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()));
    }

    @Test
    void should_rob_with_zone_id_when_get_rob_that_have_zone_id() throws Exception {
        Rob rob = new Rob("Rob", "Hall");
        String serializedRob = serialize(rob);
        mockMvc.perform(post("/api/staffs").contentType(MediaType.APPLICATION_JSON).content(serializedRob));

        Zone zone = new Zone();
        zone.setZoneId("Asia/Chongqing");
        String serializedZone = serialize(zone);
        mockMvc.perform(put("/api/staffs/1/timezone").contentType(MediaType.APPLICATION_JSON).content(serializedZone))
                .andExpect(status().is(HttpStatus.OK.value()));

        mockMvc.perform(get("/api/staffs/1"))
                .andExpect(status().is(HttpStatus.OK.value()))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.firstName").value("Rob"))
                .andExpect(jsonPath("$.lastName").value("Hall"))
                .andExpect(jsonPath("$.zoneId").value("Asia/Chongqing"));
    }

    @Test
    void should_robs_id_null_when_rob_have_not_zone_id() throws Exception {
        Rob rob = new Rob("Rob", "Hall");
        String serializedRob = serialize(rob);
        mockMvc.perform(post("/api/staffs").contentType(MediaType.APPLICATION_JSON).content(serializedRob));

        mockMvc.perform(get("/api/staffs/1"))
                .andExpect(status().is(HttpStatus.OK.value()))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.firstName").value("Rob"))
                .andExpect(jsonPath("$.lastName").value("Hall"))
                .andExpect(jsonPath("$.zoneId").doesNotExist());
    }
}
