create table IF NOT EXISTS rob (
       id bigint not null AUTO_INCREMENT PRIMARY KEY,
        first_name varchar(64) not null,
        last_name varchar(64) not null,
        zone_id varchar(255),
        primary key (id)
)
