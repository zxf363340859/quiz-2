package com.twuc.webApp.entity;

import javax.persistence.*;
import java.time.Duration;
import java.time.OffsetDateTime;

@Entity
public class Sofia {

    @Id
    @GeneratedValue
    private Long id;

    @Column(length = 128, nullable = false)
    private String username;

    @Column(length = 64, nullable = false)
    private String companyName;

    @Column(nullable = false)
    private String zoneId;

    @Column(nullable = false)
    private OffsetDateTime startTime;

    @Column(nullable = false)
    private Duration duration;

    @ManyToOne
    @JoinColumn
    private Rob rob;

    public Sofia() {
    }

    public Sofia(String username, String companyName, Rob rob) {
        this.username = username;
        this.companyName = companyName;
        this.rob = rob;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Rob getRob() {
        return rob;
    }

    public void setRob(Rob rob) {
        this.rob = rob;
    }
}
