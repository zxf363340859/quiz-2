package com.twuc.webApp.entity;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
public class Rob {

    @Id
    @GeneratedValue
    private Long id;

    @Column(length = 64, nullable = false)
    private String firstName;

    @Column(length = 64, nullable = false)
    private String lastName;

    @Column
    private String zoneId;

    @OneToMany(mappedBy = "rob")
    private List<Sofia> sofias;

    public Rob() {
    }

    public Rob(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getZoneId() {
        return zoneId;
    }

    public void setZoneId(String zoneId) {
        this.zoneId = zoneId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Rob rob = (Rob) o;
        return Objects.equals(firstName, rob.firstName) &&
                Objects.equals(lastName, rob.lastName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName);
    }

    @Override
    public String toString() {
        return "Rob{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", zoneId='" + zoneId + '\'' +
                ", sofias=" + sofias +
                '}';
    }
}
