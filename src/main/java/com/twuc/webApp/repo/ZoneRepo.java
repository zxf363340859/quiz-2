package com.twuc.webApp.repo;

import com.twuc.webApp.entity.Zone;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ZoneRepo extends JpaRepository<Zone, Long> {
}
