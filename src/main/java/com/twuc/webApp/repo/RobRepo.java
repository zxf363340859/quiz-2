package com.twuc.webApp.repo;

import com.twuc.webApp.entity.Rob;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RobRepo extends JpaRepository<Rob, Long> {
}
