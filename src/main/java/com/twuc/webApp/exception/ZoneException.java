package com.twuc.webApp.exception;

public class ZoneException extends RuntimeException{

    public ZoneException(String message) {
        super(message);
    }
}
