package com.twuc.webApp.web;

import com.twuc.webApp.entity.Rob;
import com.twuc.webApp.entity.Zone;
import com.twuc.webApp.exception.ZoneException;
import com.twuc.webApp.repo.RobRepo;
import org.hibernate.exception.DataException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.zone.ZoneRulesProvider;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@RestController
public class PersonaController {

    @Autowired
    RobRepo robRepo;

    @PostMapping("/api/staffs")
    public ResponseEntity addRob(@RequestBody Rob rob) {
        Rob save = robRepo.saveAndFlush(rob);
        return ResponseEntity.status(HttpStatus.CREATED)
                .header("Location", "/api/staffs/" + save.getId()).build();
    }

    @GetMapping("/api/staffs/{staffId}")
    public ResponseEntity<Rob> getRob(@PathVariable Long staffId){
        Optional<Rob> robOptional = robRepo.findById(staffId);
        return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON)
                .body(robOptional.get());
    }

    @GetMapping("/api/staffs")
    public ResponseEntity<List<Rob>> getAllRobs() {
        List<Rob> robs = robRepo.findAll(Sort.by("id"));
        if(robs.isEmpty()) {
            return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON)
                    .body(robs);
        }
        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON)
                .body(robs);
    }

    @PutMapping("/api/staffs/{staffId}/timezone")
    public ResponseEntity saveZoneIdForRob(@PathVariable Long staffId, @RequestBody Zone zone) {
        if(zone.getZoneId() == null || !ZoneRulesProvider.getAvailableZoneIds().contains(zone.getZoneId())) {
            throw new ZoneException("zone error");
        }
        Rob rob = robRepo.findById(staffId).get();
        rob.setZoneId(zone.getZoneId());
        robRepo.saveAndFlush(rob);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @ExceptionHandler(DataException.class)
    public ResponseEntity dataExceptionHandler() {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }

    @ExceptionHandler(NoSuchElementException.class)
    public ResponseEntity noSuchElementExceptionHandler() {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @ExceptionHandler(ZoneException.class)
    public ResponseEntity zoneExceptionHandler() {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }
}
