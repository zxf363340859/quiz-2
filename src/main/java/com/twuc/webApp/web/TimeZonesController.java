package com.twuc.webApp.web;

import com.twuc.webApp.entity.Zone;
import com.twuc.webApp.repo.ZoneRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.zone.ZoneRulesProvider;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@RestController
public class TimeZonesController {

    @Autowired
    ZoneRepo zoneRepo;


    @GetMapping("/api/timezones")
    public ResponseEntity<List<String>> getTimeZones() {
        saveZones();

        List<Zone> zones = zoneRepo.findAll(Sort.by("zoneId"));
        List<String> zoneIds = new ArrayList<>();
        zones.forEach(zone -> zoneIds.add(zone.getZoneId()));

        return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON).body(zoneIds);
    }

    public void saveZones() {
        Set<String> availableZoneIds = ZoneRulesProvider.getAvailableZoneIds();
        List<Zone> zones = new ArrayList<>();
        availableZoneIds.forEach(z -> zones.add(new Zone(z)));

        List<Zone> s = zoneRepo.saveAll(zones);
        zoneRepo.flush();
    }

}
